package jp.aegif.alfresco.repo.content.archive;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.schedule.AbstractScheduledLockedJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class IneffectiveContentArchiveJob extends AbstractScheduledLockedJob implements StatefulJob {
	@Override
	public void executeJob(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		Object ineffectiveContentArchiverObject = jobDataMap.get("ineffectiveContentArchiver");

		if (ineffectiveContentArchiverObject == null || !(ineffectiveContentArchiverObject instanceof IneffectiveContentArchiver)) {
			throw new AlfrescoRuntimeException("Invalid JobData for IneffectiveContentArchiveJob");
		}
		
		final IneffectiveContentArchiver ineffectiveContentArchiver = (IneffectiveContentArchiver)ineffectiveContentArchiverObject;
		AuthenticationUtil.runAsSystem(new RunAsWork<Object>() {
			@Override
			public Object doWork() throws Exception {
				ineffectiveContentArchiver.execute();
				return null;
			}
		});
	}
}
