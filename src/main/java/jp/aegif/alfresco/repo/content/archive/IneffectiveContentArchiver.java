package jp.aegif.alfresco.repo.content.archive;

import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IneffectiveContentArchiver {
    private static Log logger = LogFactory.getLog(IneffectiveContentArchiver.class);
    
    private SearchService searchService;
    private NodeService nodeService;
    private TransactionService transactionService;
    
    public void setSearchService(SearchService searchService) {
    	this.searchService = searchService;
    }
    
    public void setNodeService(NodeService nodeService) {
    	this.nodeService = nodeService;
    }
    
    public void setTransactionService(TransactionService transactionService) {
    	this.transactionService = transactionService;
    }
    
	public void execute() {
		transactionService.getRetryingTransactionHelper().doInTransaction(
				new RetryingTransactionCallback<Object>() {
					@Override
					public Object execute() throws Throwable {
						String query = "+ASPECT:\"cm:effectivity\" +@cm\\:to:[MIN TO NOW]";
						ResultSet rs = null;
						SearchParameters sp = new SearchParameters();
						sp.setLanguage(SearchService.LANGUAGE_LUCENE);
						sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
						sp.setQuery(query);
						rs = searchService.query(sp);
						
						for (NodeRef nodeRef : rs.getNodeRefs()) {
							nodeService.deleteNode(nodeRef);
						}
						logger.debug("Archive " + rs.length() + " nodes");

						return null;
					}
		});
	}
}
